package com.shitbot.command.impl;

import com.shitbot.Bot;
import com.shitbot.command.Command;
import com.shitbot.profile.Profile;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class GainzCommand extends Command {
    public GainzCommand() {
        super("gainz");
    }

    @Override
    public void onCommand(GuildMessageReceivedEvent event, List<String> arguments) {
        String name = String.join("", arguments);
        if (name.isEmpty()) {
            for (Profile profile : Bot.PROFILE_LOADER.loadAll()) {
                Profile.checkDaily(profile, event.getChannel());
            }
            return;
        }
        if (!Bot.PROFILE_LOADER.exists(name)) {
            event.getMessage().reply("I am not currently tracking that user.").mentionRepliedUser(true).queue();
            return;
        }
        try {
            Profile profile = Bot.PROFILE_LOADER.load(name);
            Message message = Profile.checkDaily(profile, event.getChannel()).get();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
