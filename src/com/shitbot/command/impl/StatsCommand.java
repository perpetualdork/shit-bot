package com.shitbot.command.impl;

import com.shitbot.command.Command;
import com.shitbot.util.WebUtil;
import net.dv8tion.jda.api.events.message.guild.GuildMessageReceivedEvent;

import java.util.List;

public class StatsCommand extends Command {
    public StatsCommand() {
        super("stats");
    }

    @Override
    public void onCommand(GuildMessageReceivedEvent event, List<String> arguments) {
        String name = String.join("", arguments);
        String base = "https://www.runeclan.com/user/" + name + "/media";
        //http://www.runeclan.com/signature/user/153085/user1-dark.png&quot;
        String[] lines = WebUtil.download(base).split("\n");
        for (String line : lines) {
            if (line.contains("user1-dark")) {
                event.getChannel().sendMessage(line.split("&quot;&gt;&lt;img src=&quot;")[1].split("&quot;")[0]).queue();
            }
        }
    }
}
