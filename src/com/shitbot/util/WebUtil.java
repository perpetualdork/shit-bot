package com.shitbot.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

public class WebUtil {
    public static String download(String url) {
        StringBuilder b = new StringBuilder();
        try {
            URL oracle = new URL(url);
            BufferedReader in = new BufferedReader(new InputStreamReader(oracle.openStream()));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                b.append(inputLine).append("\n");
            }
            in.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return b.toString();
    }
}
