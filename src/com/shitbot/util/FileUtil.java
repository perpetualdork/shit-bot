package com.shitbot.util;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileUtil {
    @SuppressWarnings("unchecked")

    private static Path getPath(String directory) {
        return Paths.get(directory).normalize();
    }

    public static Path getPathOrCreate(String directory) {
        return create(getPath(directory));
    }

    private static Path create(Path target) {
        try {
            if (Files.exists(target))
                return target;
            if (target.getParent() != null && !Files.exists(target.getParent()))
                Files.createDirectories(target.getParent());
            return Files.createFile(target);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}