package com.shitbot.util;

import org.joda.time.DateTime;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

public class TimeUtil {
    private static final SimpleDateFormat prettyFormat = new SimpleDateFormat("HH:mm");

    public static boolean isSameDay(Date date1, Date date2) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyyMMdd");
        return fmt.format(date1).equals(fmt.format(date2));
    }

    public static String getGameTime() {
        return prettyFormat.format(System.currentTimeMillis());
    }

    public static Date convert(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static DateTime getFirstDayOfWeek(DateTime other) {
        if (other.dayOfWeek().get() == 7)
            return other;
        else
            return other.minusWeeks(1).withDayOfWeek(7);
    }
}
