package com.shitbot.util;

import java.util.Objects;

public class StringUtil {
    public static String formatDisplayName(String name) {
        Objects.requireNonNull(name);
        name = name.replaceAll("_", " ").toLowerCase();
        StringBuilder builder = new StringBuilder();
        boolean spaced = true;
        for (int i = 0; i < name.length(); i++) {
            if (spaced) {
                builder.append(("" + name.charAt(i)).toUpperCase());
                spaced = false;
            } else
                builder.append(name.charAt(i));
            if (name.charAt(i) == ' ')
                spaced = true;
        }
        return builder.toString();
    }
}
