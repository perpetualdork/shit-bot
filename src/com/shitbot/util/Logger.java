package com.shitbot.util;

import org.jetbrains.annotations.NotNull;

import java.io.PrintStream;
import java.text.SimpleDateFormat;

public class Logger extends PrintStream {
    private static final SimpleDateFormat date = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

    private Logger() {
        super(System.out);
    }

    public static void configure() {
        System.setOut(new Logger());
    }

    private String build(Object input) {
        Throwable th = new Throwable();
        StackTraceElement[] stack = new Throwable().getStackTrace();
        StackTraceElement t = stack[2];
        if (t.getFileName() == null)
            t = stack[3];
        return "[" + date.format(System.currentTimeMillis()) + "] .(" + t.getFileName() + ":" + t.getLineNumber() + "): " + input;
    }

    @Override
    public void println(String input) {
        super.println(build(input));
    }

    @Override
    public void println(long input) {
        super.println(build(input));
    }

    @Override
    public void println(boolean input) {
        super.println(build(input));
    }

    @Override
    public void println(char input) {
        super.println(build(input));
    }

    @Override
    public void println(char @NotNull [] input) {
        super.println(build(input));
    }

    @Override
    public void println(double input) {
        super.println(build(input));
    }

    @Override
    public void println(float input) {
        super.println(build(input));
    }

    @Override
    public void println(int input) {
        super.println(build(input));
    }

    @Override
    public void println(Object input) {
        super.println(build(input));
    }
}