package com.shitbot.gainz.stat;

import com.shitbot.util.StringUtil;

public enum Stats {
    OVERALL(90, 580),
    ATTACK(80, 35),
    DEFENCE(71, 141),
    STRENGTH(71, 91),
    CONSITUTION(210, 35),
    RANGED(71, 194),
    PRAYER(71, 247),
    MAGIC(71, 300),
    COOKING(345, 198),
    WOODCUTTING(345, 304),
    FLETCHING(210, 303),
    FISHING(345, 145),
    FIREMAKING(345, 251),
    CRAFTING(210, 250),
    SMITHING(345, 92),
    MINING(345, 35),
    HERBLORE(210, 144),
    AGILITY(210, 91),
    THIEVING(210, 197),
    SLAYER(210, 356),
    FARMING(345, 357),
    RUNECRAFTING(71, 353),
    HUNTER(210, 410),
    CONSTRUCTION(71, 406),
    SUMMONING(345, 410),
    DUNGEONEERING(71, 459),
    DIVINATION(210, 467),
    INVENTION(345, 467),
    ARCHAEOLOGY(71, 512);
    private final int x, y;

    Stats(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public static Stats getStat(int i) {
        for (Stats value : values()) {
            if (value.ordinal() == i)
                return value;
        }
        return null;
    }

    @Override
    public String toString() {
        return StringUtil.formatDisplayName(name());
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
