package com.shitbot.gainz.service;

import com.shitbot.Bot;
import com.shitbot.profile.Profile;

public class ProfileUpdateService implements Runnable {
    @Override
    public void run() {
        System.out.println("Updating all records..");
        Bot.PROFILE_LOADER.loadAll().forEach(Profile::record);
    }
}
