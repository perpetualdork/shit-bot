package com.shitbot.gainz;

import com.shitbot.gainz.stat.Stats;

public class Gain {
    private final Stats stat;
    private final long levels;
    private final long experience;
    private final long ranks;
    private long totalXP;

    public Gain(Stats stat, long levels, long experience, long ranks) {
        this.stat = stat;
        this.levels = levels;
        this.experience = experience;
        this.ranks = ranks;
    }

    public Stats getStat() {
        return stat;
    }

    public long getLevels() {
        return levels;
    }

    public long getExperience() {
        return experience;
    }

    public long getRanks() {
        return ranks;
    }

    public long getTotalXP() {
        return totalXP;
    }

    public void setTotalXP(long totalXP) {
        this.totalXP = totalXP;
    }
}
